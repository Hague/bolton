
# BoltOn

A bluetooth file manager for the Wahoo ELEMNT Bolt.

Supports

* browsing, transferring, and deleting files,
* files can be routes, plans, recorded rides, &c.,
* starting routes/plans from the internal directories,
* convert FIT files into GPX when receiving (since FIT is not
  well-supported in FOSS Android).

Routes and plans can be started from their respective internal
directories. If you copy routes or plans to the USB directories, you
will need to sync via the plans menu on the Bolt. Routes are copied to
EXT_ROUTES in the internal directory. Plans, are copied to folder 3 of
the internal plans directory.

Note, if saving files directly to the internal folders, be aware that
syncing the Bolt can clear these directories (they're supposed to be
mirrors of an external provider or the USB directories).

Plans are quite simple looking text files, so it should be pretty easy
to write your own.

## F-Droid

BoltOn is [available on F-Droid][fdroid].

## Further Features

More features are supported by the [BoltBT][boltbt] Python script. These can be
ported across, though i find Android UI development quite tiresome...

## Screenshots

![Pairing with the Bolt](screenshots/pairing.png)
![Browsing Routes](screenshots/routes.png)
![Browsing Internal Routes](screenshots/internal-routes.png)
![Night Mode](screenshots/night.png)

[boltbt]: https://gitlab.com/Hague/boltbt
[fdroid]: https://f-droid.org/en/packages/com.matt.bolton/


package com.matt.bolton

import java.util.UUID

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.companion.AssociationRequest
import android.companion.BluetoothLeDeviceFilter
import android.companion.CompanionDeviceManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.ParcelUuid
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts.RequestMultiplePermissions
import androidx.activity.result.contract.ActivityResultContracts.RequestPermission
import androidx.activity.result.contract.ActivityResultContracts.StartIntentSenderForResult
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.content.IntentCompat
import androidx.core.view.WindowCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.navigation.NavigationBarView
import com.google.android.material.shape.MaterialShapeDrawable;

// keeping because finding this import is an epic battle
// import androidx.activity.viewModels

import com.matt.bolton.androidversion.versionUtils
import com.matt.bolton.bolt.BOLT_SERVICE_UUID
import com.matt.bolton.bolt.FileListingType
import com.matt.bolton.databinding.BoltActivityBinding

class BoltOnActivity : AppCompatActivity() {

    private val NOTIFICATION_CHANNEL_ID = "BoltOnNotifications"

    private val NUM_FILE_PAGES = 3
    private val pagesPosType = mapOf(
        0 to FileListingType.USB,
        1 to FileListingType.INTERNAL_ROUTES,
        2 to FileListingType.INTERNAL_PLANS
    )
    private val pagesPosMenuID = mapOf(
        0 to R.id.navigation_usb,
        1 to R.id.navigation_internal_routes,
        2 to R.id.navigation_internal_plans
    )
    private val pagesMenuIDPos
        = pagesPosMenuID.entries.associate{(k,v)-> v to k}

    private lateinit var binding : BoltActivityBinding

    private lateinit var fileListingPagerAdapter : FileListingPagerAdapter
    private var scanning = false

    private lateinit var viewModel : BoltViewModel

    // for closing from notifications
    private val INTENT_CLOSE_ACTIVITY = "android.intent.CLOSE_ACTIVITY"
    private val closeReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            finish();
        }
    }

    // for clearing notifications when device goes
    private val disconnectReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            val device : BluetoothDevice?
                = IntentCompat.getParcelableExtra(
                    intent,
                    BluetoothDevice.EXTRA_DEVICE,
                    BluetoothDevice::class.java
                )
            device?.let {
                if (it.getAddress().equals(viewModel.device?.getAddress()))
                    clearAllNotifications()
            }
        }
    }

    // for clearing notifications when bluetooth is turned off
    private val bluetoothOffReceiver = object : BroadcastReceiver() {
        override fun onReceive(context : Context, intent : Intent) {
            when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1)) {
                BluetoothAdapter.STATE_OFF -> clearAllNotifications()
            }
        }
    }


    // For POST_NOTIFICATIONS permission needed
    private val notificationPermissionLauncher : ActivityResultLauncher<String>
        = registerForActivityResult(RequestPermission(), { })

    private val isViewModelDisconnected : Boolean
        get() = (viewModel.connectionStatus.value == null ||
                 viewModel.connectionStatus.value ==
                    BoltViewModel.ConnectionStatus.DISCONNECTED)

    private val deviceManager : CompanionDeviceManager
        by lazy(LazyThreadSafetyMode.NONE) {
        getSystemService(CompanionDeviceManager::class.java)
    }

    private val enableBTLauncher = registerForActivityResult(
        object : ActivityResultContract<Unit, Boolean>() {
            override fun createIntent(context : Context, input : Unit)
                = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)

            override fun parseResult(
                resultCode : Int, intent : Intent?
            ) : Boolean
                = resultCode == AppCompatActivity.RESULT_OK
        }
    ) { result : Boolean ->
        if (result) findDevice()
        else setClickConnect()
    }

    // For BLUETOOTH_CONNECT, BLUETOOTH_SCAN
    private val bluetoothPermissionLauncher
        = registerForActivityResult(RequestMultiplePermissions(), {
            granted : Map<String, Boolean> ->
                if (versionUtils.hasBluetooth(granted)) {
                    startFindDevice()
                } else {
                    setClickConnect()
                    val dialog = BluetoothPermissionDialog()
                    dialog.show(
                        getSupportFragmentManager(), "BluetoothPermissionDialog"
                    );
                }
            }
        )

    val selectDeviceLauncher = registerForActivityResult(
        object : ActivityResultContract<IntentSender, ScanResult?>() {
            val startContract = StartIntentSenderForResult()

            override fun createIntent(
                context : Context, input : IntentSender
            ) = startContract.createIntent(
                context,
                IntentSenderRequest.Builder(input).build()
            )

            override fun parseResult(
                resultCode : Int, intent : Intent?
            ) : ScanResult? {
                return when (resultCode) {
                    AppCompatActivity.RESULT_OK ->
                        versionUtils.getBLEDeviceFromIntent(intent)
                    else -> null
                }
            }
        }
    ) { scan : ScanResult? ->
        onSelectedDevice(scan)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        binding = BoltActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBarElevation(binding.appBarLayout)

        registerReceivers()
        createNotificationChannel()
        handleBackButton()

        viewModel = ViewModelProvider(this).get(BoltViewModel::class.java)
        addSharedUrlToViewModel()

        initialiseUI()
        if (isViewModelDisconnected)
            startFindDevice()
    }

    override fun onPause() {
        super.onPause()
        notifyIfConnectionOpen()
    }

    override fun onResume() {
        super.onResume()
        clearAllNotifications()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearAllNotifications()
    }


    override fun onCreateOptionsMenu(menu : Menu) : Boolean {
        val inflater = getMenuInflater()
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu : Menu) : Boolean {
        val hasShared
            = viewModel.hasSharedContentUris()
                && !isViewModelDisconnected

        menu.findItem(R.id.send_shared)?.apply {
            setVisible(hasShared)
            setEnabled(hasShared)
        }

        return true
    }

    override fun onOptionsItemSelected(item : MenuItem) : Boolean {
        val id = item.getItemId()
        if (id == R.id.send_shared) {
            val pos = binding.fileListingPager.getCurrentItem()
            val fileListingType
                = fileListingPagerAdapter.getFileListingType(pos)
            viewModel.sendSharedContent(fileListingType)
            return true
        }
        return false
    }

    /**
     * Reset UI to click/connect
     */
    private fun setClickConnect() {
        scanning = false
        getSupportActionBar()?.title = getResources().getString(R.string.click_connect)
    }

    private fun startFindDevice() {
        if (versionUtils.needsBluetoothPermissionRequest(this)) {
            versionUtils.requestBluetooth(bluetoothPermissionLauncher)
        } else {
            scanning = true
            getSupportActionBar()?.title = getResources().getString(R.string.scanning)

            val bluetoothManager
                = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            val bluetoothAdapter = bluetoothManager.adapter

            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled) {
                enableBTLauncher.launch(Unit)
            } else {
                findDevice()
            }
        }
    }

    private fun findDevice() {
        val scanFilter : ScanFilter
            = ScanFilter.Builder()
                        .setServiceUuid(ParcelUuid(BOLT_SERVICE_UUID))
                        .build()

        val deviceFilter : BluetoothLeDeviceFilter
            = BluetoothLeDeviceFilter.Builder()
                                     .setScanFilter(scanFilter)
                                     .build()

        val pairingRequest : AssociationRequest
            = AssociationRequest.Builder()
                                .addDeviceFilter(deviceFilter)
                                .build()

        deviceManager.associate(
            pairingRequest,
            object : CompanionDeviceManager.Callback() {
                override fun onAssociationPending(
                    chooserLauncher : IntentSender
                ) {
                    selectDeviceLauncher.launch(chooserLauncher)
                }

                // functionally same as onAssociationPending, renamed in API
                // 33, keep old callback for old devices
                @Deprecated("same as onAssociationPending, for API < 33")
                override fun onDeviceFound(
                    chooserLauncher : IntentSender
                ) {
                    selectDeviceLauncher.launch(chooserLauncher)
                }

                override fun onFailure(error: CharSequence?) {
                    setClickConnect()
                }
            },
            null
        )
    }

    private fun onSelectedDevice(scan : ScanResult?) {
        checkRequestNotificationPermissions()
        scanning = false
        scan?.device?.let {
            viewModel.connect(it)
        } ?: run {
            setClickConnect()
        }
    }

    private fun onDeviceConnected() {
        try {
            invalidateOptionsMenu()
            getSupportActionBar()?.title = viewModel.device?.getName()
        } catch (e : SecurityException) {
            // should not happen if we connected!
        }
    }

    private fun onDeviceConnecting() {
        getSupportActionBar()?.title = getResources().getString(R.string.connecting)
    }

    private fun onDeviceDisconnected() {
        invalidateOptionsMenu()
        clearAllNotifications()
        if (!scanning) {
            setClickConnect()
        }
        disassociateAllDevices()
    }

    private fun disassociateAllDevices() {
        versionUtils.disassociateAllDevices(deviceManager)
    }

    private fun initialiseUI() {
        setSupportActionBar(binding.toolbar)
        binding.toolbar.setOnClickListener { _ ->
            if (isViewModelDisconnected)
                startFindDevice()
        }

        val navigation : BottomNavigationView
            = findViewById(R.id.file_list_navigation)

        // TODO: create maps from page positions to ids and vice versa?
        fileListingPagerAdapter = FileListingPagerAdapter(this)
        binding.fileListingPager.adapter = fileListingPagerAdapter
        binding.fileListingPager.registerOnPageChangeCallback(
            object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position : Int) {
                    navigation.setSelectedItemId(pagesPosMenuID[position]!!)
                }
            }
        )

        navigation.setOnItemSelectedListener(
            object : NavigationBarView.OnItemSelectedListener {
                override fun onNavigationItemSelected(
                    item : MenuItem
                ) : Boolean {
                    val pos = pagesMenuIDPos[item.getItemId()]!!
                    binding.fileListingPager.setCurrentItem(pos)
                    return true
                }
            }
        )

        viewModel.connectionStatus
                 .observe(this,
                          Observer<BoltViewModel.ConnectionStatus> { status ->
            when (status) {
                BoltViewModel.ConnectionStatus.DISCONNECTED -> {
                    onDeviceDisconnected()
                }
                BoltViewModel.ConnectionStatus.CONNECTING -> {
                    onDeviceConnecting()
                }
                BoltViewModel.ConnectionStatus.CONNECTED -> {
                    onDeviceConnected()
                }
            }
        })

        viewModel.fileActionInfo.observe(
            this,
            Observer<BoltViewModel.FileActionInfo>() { info ->
                if (info.status == BoltViewModel.FileActionStatus.FAILED) {
                    Toast.makeText(
                        this,
                        getResources().getString(R.string.file_action_failed),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                updateFileActionInfo(info)
            }
        )
    }

    private inner class FileListingPagerAdapter(fa : FragmentActivity)
            : FragmentStateAdapter(fa) {
        val fragments : MutableMap<Int, FileListingFragment> = mutableMapOf()

        override fun getItemCount() : Int = NUM_FILE_PAGES

        override fun createFragment(position : Int) : Fragment {
            val fragment = FileListingFragment()
            val bundle = Bundle()
            val listType =  getFileListingType(position)
            bundle.putSerializable(FILE_LISTING_FRAGMENT_LIST_TYPE, listType)
            fragment.arguments = bundle
            fragments[position] = fragment
            return fragment
        }

        fun getFileListingType(position : Int) : FileListingType {
            return when (position) {
                0 -> FileListingType.USB
                1 -> FileListingType.INTERNAL_ROUTES
                else -> FileListingType.INTERNAL_PLANS
            }
        }

        fun getFragment(position : Int) : FileListingFragment? {
            return fragments[position]
        }
    }

    private fun updateFileActionInfo(info : BoltViewModel.FileActionInfo) {
        when (info.status) {
            BoltViewModel.FileActionStatus.SENDING -> {
                binding.fileActionInfo.setVisibility(View.VISIBLE)
                binding.fileActionStatus.text =
                    getFileStatusText(R.string.file_sending, info.filename)
                setFileActionProgress(info.progress)
            }
            BoltViewModel.FileActionStatus.GETTING -> {
                binding.fileActionInfo.setVisibility(View.VISIBLE)
                binding.fileActionStatus.text =
                    getFileStatusText(R.string.file_getting, info.filename)
                setFileActionProgress(info.progress)
            }
            BoltViewModel.FileActionStatus.FAILED -> {
                // TODO: Toast warning
                binding.fileActionInfo.setVisibility(View.GONE)
            }
            else -> {
                binding.fileActionInfo.setVisibility(View.GONE)
            }
        }
    }

    /**
     * Set progress of file action bar
     *
     * @param progress percentage progress or -1 for indeterminate
     */
    private fun setFileActionProgress(progress : Int) {
        if (progress < 0) {
            binding.fileActionProgress.setIndeterminate(true)
        } else {
            binding.fileActionProgress.setIndeterminate(false)
            binding.fileActionProgress.progress = progress
        }
    }

    private fun getFileStatusText(prefixId : Int, filename : String?) : String {
        val cleanFilename = if (filename == null) "" else filename
        return getResources().getString(prefixId) +
               " " +
               cleanFilename
    }

    private fun createNotificationChannel() {
        val name = getString(R.string.channel_name)
        val descriptionText = getString(R.string.channel_description)
        val channel = NotificationChannel(
            NOTIFICATION_CHANNEL_ID,
            name,
            NotificationManager.IMPORTANCE_LOW
        ).apply {
            description = descriptionText
        }
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    // Lint suppressed since areNotificationsEnabled checks permission
    @SuppressLint("MissingPermission")
    private fun notifyIfConnectionOpen() {
        if (isViewModelDisconnected)
            return

        // on click, restart existing activity
        val launchIntent = Intent(this, BoltOnActivity::class.java).apply {
            setAction(Intent.ACTION_MAIN);
            addCategory(Intent.CATEGORY_LAUNCHER);
        }
        val launchPendingIntent: PendingIntent
            = PendingIntent.getActivity(
                this, 0, launchIntent, PendingIntent.FLAG_IMMUTABLE
            )

        // exit activity via button
        val closeIntent = Intent(INTENT_CLOSE_ACTIVITY)
        val closePendingIntent
            = PendingIntent.getBroadcast(
                this, 0 , closeIntent, PendingIntent.FLAG_IMMUTABLE
            );

        var builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.notification_icon)
            .setContentTitle(getString(R.string.connection_open_title))
            .setContentText(getString(R.string.connection_open_message))
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText(getString(R.string.connection_open_message)))
            .setPriority(NotificationCompat.PRIORITY_LOW)
            .setContentIntent(launchPendingIntent)
            .setAutoCancel(true)
            .addAction(R.drawable.exit_icon,
                       getString(R.string.exit),
                       closePendingIntent)

        val nm = NotificationManagerCompat.from(this)
        if (nm.areNotificationsEnabled()) {
            // TODO: notification ids properly
            // notificationId is a unique int for each notification that you must define
            nm.notify(1, builder.build())
        }
    }

    private fun clearAllNotifications() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE)
                as NotificationManager
        notificationManager.cancelAll()
    }

    private fun registerReceivers() {
        val closeFilter = IntentFilter(INTENT_CLOSE_ACTIVITY)
        ContextCompat.registerReceiver(
            this,
            closeReceiver,
            closeFilter,
            ContextCompat.RECEIVER_NOT_EXPORTED
        )

        val disconnectFilter
            = IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(disconnectReceiver, disconnectFilter)

        val bluetoothOffFilter
            = IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        registerReceiver(bluetoothOffReceiver, bluetoothOffFilter)
    }


    /**
     * Request notification permissions if needed
     *
     * E.g. not if settings block them. Doesn't ask twice in an
     * activities life.
     */
    private fun checkRequestNotificationPermissions() {
        var showRationale
            = versionUtils.shouldShowRequestNotificationPermissionRationale(this)

        if (versionUtils.hasPostNotificationsPermission(this)) {
            // nothing to do
        } else if (showRationale) {
            val dialog = NotificationPermissionDialog()
            dialog.show(
                getSupportFragmentManager(), "NotificationPermissionDialog"
            )
        } else {
            versionUtils.requestPostNotifications(
                notificationPermissionLauncher
            )
        }
    }

    /**
     * Set the elevation to match the app bar
     *
     * Uses @dimen/appBarElevation rather than the default material which is to
     * have no app bar elevation unless there is some scrolling involved.
     */
    private fun setStatusBarElevation(appBarLayout : AppBarLayout) {
        appBarLayout.setStatusBarForeground(
            MaterialShapeDrawable.createWithElevationOverlay(this)
        )
    }

    private fun handleBackButton() {
        onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    val pos = binding.fileListingPager.getCurrentItem()
                    val frag = fileListingPagerAdapter.getFragment(pos)
                    val handled = frag?.onBackPressed()
                    if (handled != true) {
                        viewModel.disconnect()
                        disassociateAllDevices()
                        finish()
                    }
                }
            }
        )
    }

    /**
     * If a file was shared with the app add it to the view model
     *
     * So it can be sent to a directory from the UI
     */
    private fun addSharedUrlToViewModel() {
        val intent = getIntent()
        val action = intent.getAction()
        if (Intent.ACTION_VIEW.equals(action)) {
            intent.getData()?.let { viewModel.setSharedContentUri(it) }
        } else if (
            Intent.ACTION_SEND.equals(action)
            && intent.hasExtra(Intent.EXTRA_STREAM)
        ) {
            IntentCompat.getParcelableExtra(
                intent, Intent.EXTRA_STREAM, Uri::class.java
            )?.let { viewModel.setSharedContentUri(it) }
        } else if (
            Intent.ACTION_SEND_MULTIPLE.equals(action)
            && intent.hasExtra(Intent.EXTRA_STREAM)
        ) {
            IntentCompat.getParcelableArrayListExtra(
                intent, Intent.EXTRA_STREAM, Uri::class.java
            )?.let { viewModel.sharedContentUris = it }
        }

        invalidateOptionsMenu()
    }

    class NotificationPermissionDialog() : DialogFragment() {
        override fun onCreateDialog(savedInstanceState : Bundle?) : Dialog {
            return MaterialAlertDialogBuilder(requireActivity())
                .setTitle(getString(R.string.notifications_request))
                .setMessage(getString(R.string.notifications_request_rationale))
                .setPositiveButton(
                    android.R.string.ok,
                    { _, _ -> requestPermissions() }
                ).create()
        }

        override fun onCancel(di : DialogInterface) {
            // the docs say we shouldn't request on cancel, but unless i
            // add a preference to remember their response here, the
            // user will be bothered every time until they go through
            // the system dialog
            //
            // (currently trying to avoid having preferences for this app)
            requestPermissions()
        }

        private fun requestPermissions() {
            val activity = getActivity()
            if (activity is BoltOnActivity) {
                versionUtils.requestPostNotifications(
                    activity.notificationPermissionLauncher
                )
            }
        }
    }

    class BluetoothPermissionDialog() : DialogFragment() {
        override fun onCreateDialog(savedInstanceState : Bundle?) : Dialog {
            return MaterialAlertDialogBuilder(requireActivity())
                .setTitle(getString(R.string.bluetooth_disabled))
                .setMessage(getString(R.string.bluetooth_denied_msg))
                .setPositiveButton(
                    R.string.exit,
                    { _, _ -> getActivity()?.let { it.finish() } }
                ).setNegativeButton(
                    R.string.android_app_settings_button,
                    { _, _ ->
                        getActivity()?.let { activity ->
                            val appPackage = activity.getPackageName()
                            val intent = Intent(
                                Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + appPackage),
                            );
                            intent.addCategory(Intent.CATEGORY_DEFAULT)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                        }
                    }
                ).create()
        }
    }
}


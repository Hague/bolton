
package com.matt.bolton.bolt

import kotlinx.coroutines.channels.ClosedReceiveChannelException

/**
 * Play a route on the Bolt
 *
 * filename is the name of the file, the directory is determined by
 * the directoryCode from PLAYABLE_ROUTES constant.
 */
public class BCmdStartRoute(val directoryCode : Short,
                            val filename : String,
                            val forwards : Boolean = true) : BoltCommand() {

    public class BCmdResStartRoute(success : Boolean,
                                   val directoryCode : Short,
                                   val filename : String,
                                   val forwards : Boolean)
            : BoltCommandResult(success)

    /**
     * Send the command to the Bolt
     *
     * @throws SecurityException if does not have bt permission
     */
    @Throws(SecurityException::class)
    override suspend fun send(bolt : BoltBT) : BCmdResStartRoute {
        var success = false

        val msgID = bolt.getNextMessageID()
        val responsePrefix = byteArrayOfInts(CMD_START_ROUTE_RES, msgID)
        val listener = BoltBytesListener(bolt,
                                         CONFIG_UUID,
                                         listOf(responsePrefix))
        try {
            listener.register()

            val msgBuffer = getBoltByteBuffer(
                10 + getStringEncodedSize(filename)
            )

            val bDirection = (if (forwards) START_ROUTE_FORWARD
                              else START_ROUTE_BACKWARD)
            msgBuffer.putIntBytes(0, bDirection, 0)
            msgBuffer.putShort(directoryCode)
            msgBuffer.putBoltString(filename)
            // last four bytes is actually the timestamp for the file,
            // but four 0s works and is easier...
            msgBuffer.putIntBytes(0, 0, 0, 0, 0)

            sendLongMessage(bolt,
                            CONFIG_UUID,
                            CMD_START_ROUTE_BODY,
                            CMD_START_ROUTE_END,
                            msgID,
                            msgBuffer.array())

            val res = listener.receive()

            success = res.last() == 0.toByte()

        } catch (e : ClosedReceiveChannelException) {
            // do nothing but return false
        } finally {
            listener.close()
        }

        return BCmdResStartRoute(success, directoryCode, filename, forwards)
    }
}


package com.matt.bolton.bolt

import java.io.InputStream
import java.io.IOException
import kotlinx.coroutines.channels.ClosedReceiveChannelException

/**
 * Send a local file to the Bolt
 *
 * outFileName should be fully qualified on the device. inFile stream should be
 * created and closed by the user.
 *
 * progressCallback takes the number of byte sent so far
 */
public class BCmdSendFile(
    val inFile : InputStream,
    val outFileName : String,
    val progressCallback : (Int) -> Unit = { _ -> }) : BoltCommand() {

    public class BCmdResSendFile(success : Boolean,
                                 val outFileName : String)
            : BoltCommand.BoltCommandResult(success) { }


    override suspend fun send(bolt : BoltBT) : BCmdResSendFile {
        val helper = SendFileHelper(bolt)
        val success = helper.sendFile()
        return BCmdResSendFile(success, outFileName)
    }

    private inner class SendFileHelper(val bolt : BoltBT) {

        val msgID = bolt.getNextMessageID()

        var thisChunk = ByteArray(FILE_CHUNK_SIZE)
        var nextChunk = ByteArray(FILE_CHUNK_SIZE)
        var totalThisSize = 0
        var thisSize = 0
        var done = false

        /**
         * Asynchronous send of file, true when done if success
         */
        suspend fun sendFile() : Boolean {
            val responsePrefix = byteArrayOfInts(CMD_FILE_RESPONSE, msgID)

            var success = false
            var sendListener : BoltBytesListener? = null
            var endListener : BoltBytesListener? = null

            // i know this try/finally doesn't guarantee closure in the case of
            // cancelled coroutines, but i don't know of a clean solution.
            try {
                sendListener = BoltBytesListener(bolt,
                                                 FILE_UUID,
                                                 listOf(responsePrefix))
                sendListener.register()
                sendFileName()

                thisSize = inFile.read(thisChunk)
                var nextSize = inFile.read(nextChunk)

                if (nextSize > 0)
                    sendMultipleChunksMsg()

                totalThisSize += thisSize

                while (nextSize > 0) {
                    sendThisChunk()
                    // should check for error response..
                    sendListener.receive()
                    sendProgress()

                    thisChunk = nextChunk
                    thisSize = nextSize
                    totalThisSize += nextSize
                    nextChunk = ByteArray(FILE_CHUNK_SIZE)
                    nextSize = inFile.read(nextChunk)
                }

                sendThisChunk(true)
                // should check for errors
                sendListener.receive()
                sendProgress()

                val doneResponse = byteArrayOfInts(CMD_FILE_DONE, 0, msgID)
                endListener = BoltBytesListener(bolt,
                                                FILE_UUID,
                                                listOf(doneResponse))
                endListener.register()
                sendEndOfChunksMessage()
                val res = endListener.receive()

                success = (res.get(3) == 0.toByte())
            } catch (e : ClosedReceiveChannelException) {
                // do nothing but return false
            } catch (e : IOException) {
                // do nothing but return false
            } finally {
                sendListener?.close()
                endListener?.close()
            }

            return success
        }

        private suspend fun sendFileName() {
            var buffer = getBoltByteBuffer(
                7 + getStringEncodedSize(outFileName)
            )
            buffer.putIntBytes(0, 2)
            buffer.putBoltString(outFileName)
            buffer.putIntBytes(0, 0, 0, 0, 0)

            sendLongMessage(bolt,
                            FILE_UUID,
                            CMD_FILE_NAME_BODY,
                            CMD_FILE_NAME_END,
                            msgID,
                            buffer.array())
        }

        private suspend fun sendMultipleChunksMsg() {
            val msg = byteArrayOfInts(8, msgID, 0, 1, 2, 0, 0, 0, 0, 0)
            bolt.sendMessage(FILE_UUID, msg)
        }

        private suspend fun sendEndOfChunksMessage() {
            val msg = byteArrayOfInts(CMD_FILE_DONE, 0, msgID)
            bolt.sendMessage(FILE_UUID, msg)
        }

        /**
         * Sends the current chunk of the file
         *
         * isLast if this is the last chunk -- needs to include file
         * size in message
         */
        private suspend fun sendThisChunk(isLast : Boolean = false) {
            val gzippedChunk = gzipBytes(thisChunk, 0, thisSize)
            var buffer = getBoltByteBuffer(4 + gzippedChunk.size)

            if (isLast) {
                buffer.putInt(totalThisSize % FILE_CHUNK_SIZE)
            } else {
                buffer.put(byteArrayOfInts(0, 1, 0, 0))
            }

            buffer.put(gzippedChunk)

            sendLongMessage(bolt,
                            FILE_UUID,
                            CMD_FILE_BODY,
                            CMD_FILE_END,
                            msgID,
                            buffer.array())
        }

        private fun sendProgress() {
            progressCallback(totalThisSize)
        }
    }
}


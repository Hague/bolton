
package com.matt.bolton.bolt

import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.nio.BufferUnderflowException
import java.nio.ByteBuffer
import kotlinx.coroutines.channels.ClosedReceiveChannelException

/**
 * Get a directory listing from the Bolt
 *
 * dirname is the fully qualified directory name on the device
 *
 * If dirname ends "//" then you get fully qualified path names
 */
public class BCmdListFiles(val dirname : String) : BoltCommand() {

    public class BoltFileEntry(val filename : String,
                               val isDirectory : Boolean,
                               val size : Int,
                               val timestamp : Int) { }


    public class BCmdResListFiles(success : Boolean,
                                  val files : List<BoltFileEntry>)
            : BoltCommand.BoltCommandResult(success) { }

    private val collector = BoltBytesCollector(CMD_FILE_LIST_RES_BODY,
                                               CMD_FILE_LIST_RES_END)

    /**
     * Send the command to the Bolt
     *
     * @throws SecurityException if does not have bt permission
     */
    @Throws(SecurityException::class)
    override suspend fun send(bolt : BoltBT) : BCmdResListFiles {
        val msgID = bolt.getNextMessageID()
        val msgBuffer = getBoltByteBuffer(6 + getStringEncodedSize(dirname))
        msgBuffer.putIntBytes(0, FILE_LISTING_ALL_CHECK, 0)
        msgBuffer.putBoltString(dirname)
        msgBuffer.putIntBytes(0, 1)

        val bodyPrefix = byteArrayOfInts(CMD_FILE_LIST_RES_BODY, msgID)
        val endPrefix = byteArrayOfInts(CMD_FILE_LIST_RES_END, msgID)

        val listener = BoltBytesListener(bolt,
                                         FILE_UUID,
                                         listOf(bodyPrefix, endPrefix))
        try {
            listener.register()

            sendLongMessage(bolt,
                            FILE_UUID,
                            CMD_FILE_LIST_BODY,
                            CMD_FILE_LIST_END,
                            msgID,
                            msgBuffer.array())

            val listingBytes = collector.getFullMessage(listener)
            val listing = parseListing(listingBytes)

            bolt.sendMessage(FILE_UUID, byteArrayOfInts(3, 0))

            if (listing != null)
                return BCmdResListFiles(true, listing)
        } catch (e : ClosedReceiveChannelException) {
            // do nothing but return false
        } finally {
            listener.close()
        }

        return BCmdResListFiles(false, listOf())
    }

    /**
     * Format is 00 01 00 <dirname utf-8> 00 <int16 num files> (01
     * <filename utf-8> 00 <int32 date> <int32 size>)*
     */
    private fun parseListing(msg : ByteArray) : List<BoltFileEntry>? {
        try {
            val buffer = getBoltByteBuffer(msg)

            // skip sequencing info
            for (i in 1..3)
                buffer.get()

            if (!buffer.getSequence(0, FILE_LISTING_ALL_CHECK, 0))
                return null

            val sentDirname = buffer.getBoltNullString()
            if (!sentDirname.startsWith(dirname))
                return null

            val numFiles = buffer.getShort()
            val listing : MutableList<BoltFileEntry> = mutableListOf()

            for (i in 1..numFiles) {
                // get type 01 means just list, 03 has a checksum??
                val format = buffer.get()
                val filename = buffer.getBoltNullString()

                val size = buffer.getInt()
                val timestamp = buffer.getInt()

                if (format == FILE_LIST_FORMAT_CHECK)
                    buffer.getBoltNullString()

                val isDirectory = (format == FILE_LIST_FORMAT_DIR)

                listing.add(
                    BoltFileEntry(filename, isDirectory, timestamp, size)
                )
            }

            return listing
        } catch (e : BufferUnderflowException) {
            return null
        }
    }
}

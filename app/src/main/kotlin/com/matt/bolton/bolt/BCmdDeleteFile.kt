
package com.matt.bolton.bolt

import kotlinx.coroutines.channels.ClosedReceiveChannelException

/**
 * Request a delete of a file from the Bolt
 *
 * filename should be fully qualified on the device.
 */
public class BCmdDeleteFile(val filename : String) : BoltCommand() {

    public class BCmdResDeleteFile(success : Boolean,
                                   val filename : String)
            : BoltCommand.BoltCommandResult(success) { }


    /**
     * Send the command to the Bolt
     *
     * @throws SecurityException if does not have bt permission
     */
    @Throws(SecurityException::class)
    override suspend fun send(bolt : BoltBT) : BCmdResDeleteFile {
        var success = false
        val msgID = bolt.getNextMessageID()
        var listener : BoltBytesListener? = null

        try {
            val responsePrefix = byteArrayOfInts(CMD_FILE_INFO, msgID)
            listener = BoltBytesListener(bolt,
                                         FILE_UUID,
                                         listOf(responsePrefix))
            listener.register()

            val buffer = getBoltByteBuffer(7 + getStringEncodedSize(filename))
            buffer.putIntBytes(0, FILE_ACTION_DEL)
            buffer.putBoltString(filename)
            buffer.putIntBytes(0, 0, 0, 0, 0)

            sendLongMessage(bolt,
                            FILE_UUID,
                            CMD_FILE_NAME_BODY,
                            CMD_FILE_NAME_END,
                            msgID,
                            buffer.array())

            // Don't know what an error response looks like -- any
            // response considered good.
            listener.receive()

            success = true
        } catch (e : ClosedReceiveChannelException) {
            /* do nothing */
        } finally {
            listener?.close()
        }

        return BCmdResDeleteFile(success, filename)
    }
}


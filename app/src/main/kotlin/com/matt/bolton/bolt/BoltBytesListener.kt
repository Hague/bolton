
package com.matt.bolton.bolt

import java.util.UUID
import java.io.Closeable
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ClosedReceiveChannelException

/**
 * To receive certain prefixed message notifications from the bolt
 *
 * The offer method is designed to be called from a callback. If the
 * item can't be added directly to the queue, the listener is closed down
 * as correct message ordering can't be guaranteed after that.
 */
public class BoltBytesListener(val bolt : BoltBT,
                               val uuid : UUID,
                               val prefixes : List<ByteArray>)
        : Closeable {

    private val channel = Channel<ByteArray>(Channel.UNLIMITED)
    private var isOpen = true

    /**
     * Register this listener with the bolt
     *
     * @throws SecurityException if missing bt permission
     */
    @Throws(SecurityException::class)
    suspend fun register() {
        bolt.registerListener(this)
    }

    /**
     * Stop listening for messages
     *
     * @throws SecurityException if missing bt permission
     */
    @Throws(SecurityException::class)
    override fun close() {
        if (isOpen) {
            isOpen = false
            bolt.unregisterListener(this)
            channel.close()
        }
    }

    /**
     * Handle reception of a message synchronously
     *
     * returns false and closes listener if failed.
     */
    fun trySend(msg : ByteArray) : Boolean {
        var res = false
        if (isOpen) {
            res = !channel.trySend(msg).isFailure
            if (!res)
                close()
        }
        return res
    }

    /**
     * Await next bytes message.
     *
     * Throws exception if has been closed.
     */
    @Throws(ClosedReceiveChannelException::class)
    suspend fun receive() : ByteArray {
        return channel.receive()
    }
}




package com.matt.bolton.bolt

import java.io.Closeable
import java.util.UUID

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCallback
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothProfile
import android.content.Context
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

import com.matt.bolton.androidversion.versionUtils

public class BoltBT : Closeable, CoroutineScope {

    public class BoltBTException(msg : String) : Exception(msg) { }

    private val job : Job = Job()
    // only one bluetooth operation at a time
    private val mutex = Mutex()

    override val coroutineContext : CoroutineContext
        get() = Dispatchers.IO + job

    private val KEEP_ALIVE_TIME = 10000L
    private val MAX_MESSAGE_ID = 255

    // set to null when closed
    private var peripheral : BluetoothGatt? = null
    private val distributor = BoltBTNotificationDistributer()
    // list of UUIDs for which notifacations need to be enabled once
    // connected
    private val pendingActivations = HashSet<UUID>()
    private val activatedUUIDs = HashSet<UUID>()
    private var nextMessageId : Int = 0
    // from UUID to characteristic
    private val characteristics = HashMap<UUID, BluetoothGattCharacteristic>()
    private var disconnectCallback : Runnable? = null

    private val writeResponses = Channel<Boolean>(Channel.UNLIMITED)

    /**
     * Connect to the device with the given device context, returns true on success
     */
    suspend fun connect(device : BluetoothDevice,
                        context : Context,
                        disconnectCallback : Runnable) : Boolean {
        this.disconnectCallback = disconnectCallback
        mutex.withLock {
            val result : Boolean = suspendCoroutine { cont ->
                var resumed = false
                val gattCallback = object : BluetoothGattCallback() {
                    override fun onConnectionStateChange(
                        gatt : BluetoothGatt, status : Int, newState : Int
                    ) {
                        if (!resumed) {
                            if (newState ==
                                BluetoothProfile.STATE_CONNECTED) {
                                try {
                                    gatt.discoverServices()
                                } catch (e : SecurityException) {
                                    resumed = true
                                    cont.resume(false)
                                }
                            } else {
                                resumed = true
                                cont.resume(false)
                            }
                        } else {
                            if (newState ==
                                BluetoothProfile.STATE_DISCONNECTED) {
                                close()
                            }
                        }
                    }

                    override fun onServicesDiscovered(
                        gatt : BluetoothGatt, status : Int
                    ) {
                        if (resumed)
                            return

                        resumed = true
                        if (status == BluetoothGatt.GATT_SUCCESS) {
                            peripheral = gatt
                            cont.resume(true)
                        } else {
                            cont.resume(false)
                        }
                    }

                    override fun onCharacteristicChanged(
                        gatt : BluetoothGatt,
                        characteristic : BluetoothGattCharacteristic,
                        value : ByteArray,
                    ) {
                        // println("BOLTBT " +
                        //         characteristic.uuid + " : " +
                        //         readableBytes(characteristic.value))
                        distributor.handleNotification(
                            characteristic.uuid, value
                        )
                    }

                    @Deprecated("Only called by API < 33")
                    @Suppress("deprecation")
                    override fun onCharacteristicChanged(
                        gatt : BluetoothGatt,
                        characteristic : BluetoothGattCharacteristic
                    ) {
                        distributor.handleNotification(
                            characteristic.uuid, characteristic.value
                        )
                    }

                    override fun onCharacteristicWrite(
                        gatt : BluetoothGatt,
                        characteristic : BluetoothGattCharacteristic,
                        status : Int) {
                        // just wait for result for now
                        postResponse()
                    }

                    override fun onDescriptorWrite(
                        gatt : BluetoothGatt,
                        descriptor : BluetoothGattDescriptor,
                        status : Int) {
                        // just wait for result for now
                        postResponse()
                    }
                }

                try {
                    device.connectGatt(context, false, gattCallback)
                } catch (e : SecurityException) {
                    cont.resume(false)
                }
            }

            if (result) {
                try {
                    initialiseCharacteristics()
                    clearPendingActivations()
                } catch (e : SecurityException) {
                    // will not happen if connect was successful
                }
            }

            launch { keepAliveLoop() }

            return result
        }
    }

    val isOpen : Boolean
        get() = peripheral != null

    /**
     * Close down the interaction
     */
    override fun close() {
        job.cancel()
        try {
            peripheral?.disconnect()
        } catch (e : SecurityException) {
            // ignore, should have permission for this device by now!
        }
        peripheral = null
        disconnectCallback?.run()
        disconnectCallback = null
    }

    /**
     * Returns a new unique(ish) number that can be used as a message ID
     *
     * Not fully unique since it wraps at 255
     */
    fun getNextMessageID() : Int {
        nextMessageId = (nextMessageId + 1) % (MAX_MESSAGE_ID + 1)
        return nextMessageId
    }

    /**
     * Writes raw bytes to the characteristic with the given UUID
     *
     * Returns whether succeeded
     */
    @Throws(BoltBTException::class)
    suspend fun sendMessage(uuid : UUID, msg : ByteArray) : Boolean {
        mutex.withLock {
            if (!isOpen)
                throw BoltBTException("Can't send to unconnected device")
            if (!characteristics.containsKey(uuid))
                throw BoltBTException("Unknown characteristic UUID " + uuid)

            try {
                var success = false
                characteristics.get(uuid)?.let { characteristic ->
                    success = versionUtils.writeCharacteristic(
                        peripheral,
                        characteristic,
                        msg,
                        BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
                    ) ?: false
                }
                if (success) {
                    // just wait for now
                    awaitResponse()
                }
                return success
            } catch (e : Exception) {
                return false;
            }
        }
    }

    /**
     * Registers a listener of bolt notifications
     *
     * @throws SecurityException if don't have bt dev permission
     */
    @Throws(SecurityException::class)
    suspend fun registerListener(listener : BoltBytesListener) {
        mutex.withLock {
            activateUUID(listener.uuid)
        }
        distributor.registerListener(listener)
    }

    /**
     * Unregisters a listener of bolt byte messages
     */
    fun unregisterListener(listener : BoltBytesListener) {
        // mutex not needed
        distributor.unregisterListener(listener)
    }

    private fun initialiseCharacteristics() {
        val services = peripheral?.getServices()
        if (services != null) {
            for (service in services) {
                for (characteristic in service.getCharacteristics()) {
                    characteristics.put(characteristic.uuid, characteristic)
                }
            }
        }
    }

    /**
     * Clear list of UUIDs to activate once connected
     *
     * @throws SecurityException if don't have bt dev permission
     */
    @Throws(SecurityException::class)
    private suspend fun clearPendingActivations() {
        if (!isOpen)
            return
        for (uuid in pendingActivations)
            activateUUID(uuid)
        pendingActivations.clear()
    }

    /**
     * Activate a UUID for communication
     *
     * @throws SecurityException if don't have permission for bt dev
     */
    @Throws(SecurityException::class)
    private suspend fun activateUUID(uuid : UUID) {
        if (!characteristics.containsKey(uuid))
            return

        if (activatedUUIDs.contains(uuid))
            return

        if (isOpen) {
            peripheral?.let { nonNullPeripheral ->
                characteristics.get(uuid)?.let { characteristic ->
                    nonNullPeripheral.setCharacteristicNotification(
                        characteristic, true
                    )
                    characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG)?.let {
                        descriptor ->
                            val res = versionUtils.writeDescriptor(
                                nonNullPeripheral,
                                descriptor,
                                BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE,
                            )
                            if (res) {
                                // wait for a response
                                awaitResponse()
                                activatedUUIDs.add(uuid)
                            }
                    }
                }
            }
        } else {
            pendingActivations.add(uuid)
        }
    }


    private suspend fun awaitResponse() {
        writeResponses.receive()
    }

    private fun postResponse() {
        writeResponses.trySend(true)
    }

    private suspend fun keepAliveLoop() {
        while (isOpen) {
            if (!sendMessage(KEEP_ALIVE_UUID, KEEP_ALIVE_MSG))
                close()
            else
                delay(KEEP_ALIVE_TIME)
        }
    }
}

/**
 * Node in notification distribution tree
 */
private class ByteNode {

    val listeners : MutableList<BoltBytesListener> = mutableListOf()
    val children = HashMap<Byte, ByteNode>()

    val isEmpty : Boolean
        get() = listeners.isEmpty() && children.isEmpty()

    /**
     * Register listener to listen to byte at given prefix index
     *
     * If index is after prefix, root level listener. Prefix is arg as
     * listeners may have several
     */
    fun registerListener(listener : BoltBytesListener,
                         prefix : ByteArray,
                         index : Int) {
        if (index < prefix.size) {
            val switchByte = prefix.get(index)
            if (!children.containsKey(switchByte))
                children.put(switchByte, ByteNode())
            children.get(switchByte)?.registerListener(listener,
                                                       prefix,
                                                       index + 1)
        } else {
            listeners.add(listener)
        }
    }

    /* As register, but unregister */
    fun unregisterListener(listener : BoltBytesListener,
                           prefix : ByteArray,
                           index : Int) {
        if (index < prefix.size) {
            val switchByte = prefix.get(index)
            if (children.containsKey(switchByte)) {
                val child = children.get(switchByte)
                child?.unregisterListener(listener,
                                          prefix,
                                          index + 1)
                if (child?.isEmpty ?: false)
                    children.remove(switchByte)
            }
        } else {
            listeners.remove(listener)
        }
    }

    /**
     * Distribute the message, switching on byte at pos index
     */
    fun distribute(msg : ByteArray, index : Int) {
        for (listener in listeners)
            listener.trySend(msg)

        if (index < msg.size) {
            val switchByte = msg.get(index)
            children.get(switchByte)?.distribute(msg, index + 1)
        }
    }
}


/**
 * Handles distribution of incoming notifications
 */
private class BoltBTNotificationDistributer {

    val distributionTree = HashMap<UUID, ByteNode>()

    private var handlingNotification = false
    private val pendingUnregistrations : MutableList<BoltBytesListener>
        = mutableListOf()

    fun handleNotification(uuid : UUID, msg : ByteArray) {
        handlingNotification = true
        distributionTree.get(uuid)?.distribute(msg, 0)
        handlingNotification = false
        clearUnregistrations()
    }

    /**
     * Register a listener to receive notification messages
     */
    fun registerListener(listener : BoltBytesListener) {
        if (!distributionTree.containsKey(listener.uuid))
            distributionTree.put(listener.uuid, ByteNode())
        for (prefix in listener.prefixes) {
            distributionTree.get(listener.uuid)
                            ?.registerListener(listener, prefix, 0)
        }
    }

    fun unregisterListener(listener : BoltBytesListener) {
        if (!handlingNotification)
            doUnregisterListener(listener)
        else
            addPendingUnregister(listener)
    }

    private fun doUnregisterListener(listener : BoltBytesListener) {
        val byteNode = distributionTree.get(listener.uuid)
        for (prefix in listener.prefixes)
            byteNode?.unregisterListener(listener, prefix, 0)
        if (byteNode?.isEmpty ?: false)
            distributionTree.remove(listener.uuid)
    }

    private fun addPendingUnregister(listener : BoltBytesListener) {
        pendingUnregistrations.add(listener)
    }

    private fun clearUnregistrations() {
        if (handlingNotification)
            return

        for (listener in pendingUnregistrations)
            doUnregisterListener(listener)
    }
}


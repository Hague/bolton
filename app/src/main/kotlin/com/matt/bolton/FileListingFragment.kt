
package com.matt.bolton

import java.text.DateFormat
import java.util.Date

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts.CreateDocument
import androidx.activity.result.contract.ActivityResultContracts.GetContent
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.launch

import com.matt.bolton.androidversion.versionUtils
import com.matt.bolton.bolt.FileListingType
import com.matt.bolton.databinding.FileEntryBinding
import com.matt.bolton.databinding.FragmentFileListingBinding

val FILE_LISTING_FRAGMENT_LIST_TYPE = "listType"

public class FileListingFragment() : Fragment() {

    private val REQUEST_FILE_TO_SEND = 101
    private val REQUEST_FILE_TO_GET = 102

    private val DATE_FORMAT = "HH:mm, d MMM yyyy"

    private val GENERIC_MIME_TYPE = "application/octet-stream"

    private lateinit var viewModel : BoltViewModel
    private lateinit var listType : FileListingType

    private lateinit var binding : FragmentFileListingBinding
    private lateinit var fileListingAdapter : FileListingAdapter
    // don't allow dir changes while waiting
    private var waitingDirChange = false

    private val isViewModelConnected
        get() = (viewModel.connectionStatus.value ==
                 BoltViewModel.ConnectionStatus.CONNECTED)


    val pickFileToSendLauncher
        = registerForActivityResult(GetContent()) { uri : Uri? ->
            onFileToSend(uri)
        }
    // use a generic mime type as we could be getting any kind of file
    // over bluetooth
    val pickFileToGetLauncher
        = registerForActivityResult(CreateDocument(GENERIC_MIME_TYPE)) {
            uri : Uri? -> onFileToGet(uri)
        }

    override fun onCreateView(inflater : LayoutInflater,
                              container : ViewGroup?,
                              savedInstanceState : Bundle?) : View {
        binding = FragmentFileListingBinding.inflate(
            inflater, container, false
        )

        val listTypeArg = versionUtils.getSerializable(
            getArguments(),
            FILE_LISTING_FRAGMENT_LIST_TYPE,
            FileListingType::class.java
        )
        if (listTypeArg == null)
            return binding.root
        listType = listTypeArg

        val provider = ViewModelProvider(requireActivity())
        viewModel = provider.get(BoltViewModel::class.java)

        binding.backButton.setOnClickListener { _ -> popDirectory() }
        binding.sendButton.setOnClickListener { _ -> pickFileToSend() }

        binding.swipeContainer.setOnRefreshListener(
             object : SwipeRefreshLayout.OnRefreshListener {
                 override fun onRefresh() {
                     viewModel.refreshFileListing(listType)
                 }
             }
         )

        val context = binding.root.getContext()
        val layoutManager = LinearLayoutManager(context)
        fileListingAdapter= FileListingAdapter()
        with (binding.fileListingView) {
            setLayoutManager(layoutManager)
            setItemAnimator(DefaultItemAnimator())
            addItemDecoration(DividerItemDecoration(
                context, DividerItemDecoration.VERTICAL
            ))
            adapter = fileListingAdapter
        }

        viewModel.getFileListingLiveData(listType).observe(
            viewLifecycleOwner,
            Observer<BoltViewModel.BoltListing> { listing ->
                binding.swipeContainer.setRefreshing(false);
                onUpdatedListing(listing)
            }
        )
        viewModel.connectionStatus
                 .observe(viewLifecycleOwner,
                          Observer<BoltViewModel.ConnectionStatus> { _ ->
            setConnectionUI()
        })
        setConnectionUI()
        viewModel.isFileActionActive.observe(
            viewLifecycleOwner,
            { active -> showHideSendButton(!active) }
        )

        return binding.root
    }

    /**
     * Goes up a directory, returns false if at top
     */
    fun onBackPressed() : Boolean {
        return viewModel.popDirectory(listType)
    }

    private fun onUpdatedListing(listing : BoltViewModel.BoltListing) {
        waitingDirChange = false
        if (!listing.hasParent) {
            with (binding) {
                backButton.setImageResource(R.drawable.home_icon)
                backButton.contentDescription = "" // none needed
                pathView.text = getResources().getString(R.string.top_level)
            }
        } else {
            with (binding) {
                backButton.setImageResource(R.drawable.back_icon)
                backButton.contentDescription = getResources().getString(
                    R.string.directory_back_description
                )
                pathView.text = listing.path
            }
        }
        fileListingAdapter.fileListing = listing.files
    }

    /**
     * Adapt UI based on whether connected
     */
    private fun setConnectionUI() {
        if (isViewModelConnected) {
            waitingDirChange = true
            viewModel.refreshFileListing(listType)
            showHideSendButton(true)
        } else {
            showHideSendButton(false)
        }
    }

    private fun pickFileToSend() {
        pickFileToSendLauncher.launch("*/*")
    }

    private fun onFileToSend(uri : Uri?) {
        uri?.let { viewModel.sendFile(it, listType) }
    }

    /**
     * Start choosing where to save a file
     *
     * Save fileEntry to the chosen path
     */
    private fun pickFileToGet(fileEntry : BoltViewModel.BoltFileEntry) {
        lifecycleScope.launch {
            val convert = askFITToGPX(fileEntry.filename)
            val suggestedFilename
                = if (convert)
                    viewModel.changeFITExtensionToGPX(fileEntry.filename)
                else
                    fileEntry.filename
            saveInfile(fileEntry, convert)
            pickFileToGetLauncher.launch(suggestedFilename)
        }
    }

    /**
     * Perform the get action when file selected
     *
     * Relies on inFilename having been stored viewModel
     * before calling the file pick activity
     */
    private fun onFileToGet(uri : Uri?) {
        getSavedInFile()?.let { inFile ->
            uri?.let { uri ->
                viewModel.getFile(inFile, uri, getSavedFitToGpx())
            }
        }
    }

    /**
     * Show or hide send button
     *
     * Will always hide if no connection
     */
    private fun showHideSendButton(show : Boolean) {
        if (show && isViewModelConnected)
            binding.sendButton.setVisibility(View.VISIBLE)
        else
            binding.sendButton.setVisibility(View.GONE)
    }

    private fun pushDirectory(dirname : String) {
        if (waitingDirChange)
            return
        waitingDirChange = true
        viewModel.pushDirectory(listType, dirname)
    }

    private fun popDirectory() {
        if (waitingDirChange)
            return
        waitingDirChange = false
        viewModel.popDirectory(listType)
    }

    /**
     * Before starting pick get file activity, call this
     *
     * Needs to store file so it's still there
     * if our activity gets killed.
     */
    private fun saveInfile(
        fileEntry : BoltViewModel.BoltFileEntry,
        fitToGpx : Boolean
    ) {
        viewModel.inFileForGetting = fileEntry
        viewModel.inFileFitToGpx = fitToGpx
    }

    /**
     * Read inFile to get from view model
     *
     * Should have been stored there before starting pick get file
     * activity. Null if none set.
     */
    private fun getSavedInFile() : BoltViewModel.BoltFileEntry? {
        return viewModel.inFileForGetting
    }

    private fun getSavedFitToGpx() : Boolean {
        return viewModel.inFileFitToGpx
    }

    private fun clearSavedInFilename() {
        viewModel.inFileForGetting = null
    }

    private fun deleteFile(fileEntry : BoltViewModel.BoltFileEntry) {
        lifecycleScope.launch {
            if (checkDeleteFile(fileEntry))
                viewModel.deleteFile(listType, fileEntry)
        }
    }

    private fun playFile(fileEntry : BoltViewModel.BoltFileEntry) {
        lifecycleScope.launch {
            viewModel.playFile(fileEntry)
        }
    }

    private suspend fun
    checkDeleteFile(fileEntry : BoltViewModel.BoltFileEntry) : Boolean {
        return suspendCoroutine { cont ->
            val msg = (getResources().getString(R.string.confirm_delete) +
                       " " +
                       fileEntry.filename)

            MaterialAlertDialogBuilder(requireActivity())
               .setMessage(msg)
               .setIcon(android.R.drawable.ic_dialog_alert)
               .setPositiveButton(
                   android.R.string.ok, { _, _ -> cont.resume(true) }
               ).setNegativeButton(
                   android.R.string.cancel, { _, _ -> cont.resume(false) }
               ).show()
        }
    }

    private inner class FileListingAdapter
            : RecyclerView.Adapter<FileEntryHolder>() {

        var fileListing : List<BoltViewModel.BoltFileEntry> = listOf()
            @SuppressLint("NotifyDataSetChanged")
            set(value) {
                field = value.sortedWith(
                    compareBy({ !it.isDirectory }, { it.filename })
                )
                selectedPosition = -1
                notifyDataSetChanged()
            }

        var selectedPosition : Int = -1
            set(value) {
                if (field > -1)
                    notifyItemChanged(field)
                field = value
                notifyItemChanged(field)
            }

        override fun onCreateViewHolder(parent : ViewGroup,
                                        viewType : Int) : FileEntryHolder {
            val inflater = parent.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            val fileEntryBinding = FileEntryBinding.inflate(
                inflater, parent, false
            )
            return FileEntryHolder(fileEntryBinding, this)
        }

        override fun onBindViewHolder(holder : FileEntryHolder,
                                      position : Int) {
            holder.fileEntry = fileListing.get(position)
        }

        override fun getItemCount() : Int {
            return fileListing.size
        }

        fun toggleSelectedPosition(position : Int) {
            if (selectedPosition == position)
                selectedPosition = -1
            else
                selectedPosition = position
        }
    }

    private inner class FileEntryHolder(
        val fileEntryBinding : FileEntryBinding,
        val adapter : FileListingAdapter,
    ) : RecyclerView.ViewHolder(fileEntryBinding.root) {

        var fileEntry : BoltViewModel.BoltFileEntry? = null
            set(value) {
                field = value
                setGetFileVisibility()
                setPlayVisibility()
                setIcon()
                fileEntryBinding.fileName.text = value?.filename
                setDetails()
            }

        init {
            fileEntryBinding.root.setOnClickListener({ _ ->
                adapter.toggleSelectedPosition(getBindingAdapterPosition())
                if (fileEntry?.isDirectory == true) {
                    fileEntry?.filename?.let { pushDirectory(it) }
                }
            })
            fileEntryBinding.getFileIcon.setOnClickListener({ _ ->
                fileEntry?.let({
                    pickFileToGet(it)
                })
            })
            fileEntryBinding.playFile.setOnClickListener({ _ ->
                fileEntry?.let({ playFile(it) })
            })
            fileEntryBinding.entryIcon.setOnClickListener({ _ ->
                if (canDelete()) {
                    fileEntry?.let({ deleteFile(it) })
                }
            })
            viewModel.fileActionInfo.observe(
                viewLifecycleOwner,
                Observer<BoltViewModel.FileActionInfo> { _ ->
                    setGetFileVisibility()
                }
            )
        }

        private fun setGetFileVisibility() {
            if (!isViewModelConnected ||
                viewModel.isFileActionActive.value ?: false ||
                fileEntry?.isDirectory ?: true) {
                fileEntryBinding.getFileIcon.setVisibility(View.INVISIBLE)
            } else {
                fileEntryBinding.getFileIcon.setVisibility(View.VISIBLE)
            }
        }

        private fun setPlayVisibility() {
            if (!isViewModelConnected || !(fileEntry?.isPlayable ?: false))
                fileEntryBinding.playFile.setVisibility(View.GONE)
            else
                fileEntryBinding.playFile.setVisibility(View.VISIBLE)
        }

        private fun setIcon() {
            if (canDelete()) {
                fileEntryBinding.entryIcon
                    .setImageResource(R.drawable.delete_icon)
                fileEntryBinding.entryIcon.contentDescription =
                    getResources().getString(R.string.delete_description)
            } else if (fileEntry?.isDirectory == true) {
                fileEntryBinding.entryIcon
                    .setImageResource(R.drawable.directory_icon)
                fileEntryBinding.entryIcon.contentDescription =
                    getResources().getString(R.string.directory_description)
            } else {
                fileEntryBinding.entryIcon
                    .setImageResource(R.drawable.file_icon)
                fileEntryBinding.entryIcon.contentDescription =
                    getResources().getString(R.string.file_description)
            }
        }

        private fun canDelete() : Boolean {
            return (fileEntry?.isDirectory == false &&
                    getBindingAdapterPosition() == adapter.selectedPosition)
        }

        private fun setDetails() {
            fileEntry?.let {
                lateinit var size : String
                if (it.isDirectory) {
                    var entries = getResources().getString(R.string.entries)
                    size = "%d %s".format(it.size, entries)
                } else {
                    if (it.size < 1000)
                        size = "%dB".format(it.size)
                    else if (it.size < 1000000)
                        size = "%.1fKB".format(it.size / 1000.0)
                    else
                        size = "%.1fMB".format(it.size / 1000000.0)
                }

                val date =
                DateFormat.getDateInstance().format(
                    Date(it.timestamp * 1000L)
                )

                fileEntryBinding.fileDetails.text = String.format(
                    getResources().getString(R.string.file_details),
                    size, date
                )
            }
        }
    }

    /**
     * Ask user if should covert FIT to GPX
     *
     * Returns yes or no
     */
    private suspend fun askFITToGPX(inFilename : String) : Boolean {
        if (!viewModel.isFITFile(inFilename))
            return false

        return suspendCoroutine { cont ->
            MaterialAlertDialogBuilder(requireActivity())
                .setTitle(R.string.ask_fit_to_gpx_title)
                .setMessage(R.string.ask_fit_to_gpx)
                .setPositiveButton(
                    R.string.yes, { _, _ -> cont.resume(true) }
                ).setNegativeButton(
                    R.string.no, { _, _ -> cont.resume(false) }
                ).show()
        }
    }
}


package com.matt.bolton

import android.app.Application
import com.google.android.material.color.DynamicColors

class BoltOnApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        DynamicColors.applyToActivitiesIfAvailable(this)
    }
}


package com.matt.bolton.androidversion

import java.io.Serializable

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.BluetoothStatusCodes
import android.companion.CompanionDeviceManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

@TargetApi(Build.VERSION_CODES.TIRAMISU)
open class TiramisuUtils() : SnowConeUtils() {
    override fun <T : Serializable> getSerializable(
        bundle : Bundle?, key : String, clazz : Class<T>
    ) : T? = bundle?.getSerializable(key, clazz)

    override fun writeCharacteristic(
        bluetoothGatt : BluetoothGatt?,
        characteristic : BluetoothGattCharacteristic,
        value : ByteArray,
        writeType : Int,
    ) : Boolean? {
        try {
            return bluetoothGatt?.writeCharacteristic(
                    characteristic, value, writeType
                ) == BluetoothStatusCodes.SUCCESS
        } catch (e : SecurityException) {
            return false
        }
    }

    override fun writeDescriptor(
        bluetoothGatt : BluetoothGatt,
        descriptor : BluetoothGattDescriptor,
        value : ByteArray,
    ) : Boolean {
        try {
            return bluetoothGatt.writeDescriptor(
                    descriptor, value
                ) == BluetoothStatusCodes.SUCCESS
        } catch (e : SecurityException) {
            return false
        }
    }

    override fun hasPostNotificationsPermission(context : Context) : Boolean {
        return ContextCompat.checkSelfPermission(
            context, Manifest.permission.POST_NOTIFICATIONS
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun requestPostNotifications(
        launcher : ActivityResultLauncher<String>,
    ) {
        launcher.launch(Manifest.permission.POST_NOTIFICATIONS)
    }

    override fun shouldShowRequestNotificationPermissionRationale (
        activity : Activity,
    ) : Boolean {
        return ActivityCompat.shouldShowRequestPermissionRationale(
            activity, Manifest.permission.POST_NOTIFICATIONS
        )
    }

    override fun disassociateAllDevices(
        deviceManager : CompanionDeviceManager,
    ) {
        for (oldMac in deviceManager.getMyAssociations())
            deviceManager.disassociate(oldMac.getId())
    }
}

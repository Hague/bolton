
package com.matt.bolton.androidversion

import android.Manifest
import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.ContextCompat

@TargetApi(Build.VERSION_CODES.S)
open class SnowConeUtils() : OreoUtils() {
    override fun needsBluetoothPermissionRequest(context : Context) : Boolean {
        val scan = ContextCompat.checkSelfPermission(
            context, Manifest.permission.BLUETOOTH_SCAN
        ) == PackageManager.PERMISSION_GRANTED
        val connect = ContextCompat.checkSelfPermission(
            context, Manifest.permission.BLUETOOTH_CONNECT
        ) == PackageManager.PERMISSION_GRANTED
        return !(scan && connect)
    }

    override fun requestBluetooth(
        launcher : ActivityResultLauncher<Array<String>>,
    ) {
        launcher.launch(arrayOf(
            Manifest.permission.BLUETOOTH_SCAN,
            Manifest.permission.BLUETOOTH_CONNECT,
        ))
    }

    override fun hasBluetooth(granted : Map<String, Boolean>) : Boolean {
        val scan = granted.get(
                Manifest.permission.BLUETOOTH_SCAN
            ) ?: false
        val connect = granted.get(
                Manifest.permission.BLUETOOTH_CONNECT
            ) ?: false
        return scan && connect
    }
}

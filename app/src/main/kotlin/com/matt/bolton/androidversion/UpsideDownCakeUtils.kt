
package com.matt.bolton.androidversion

import android.annotation.TargetApi
import android.bluetooth.le.ScanResult
import android.companion.AssociationInfo
import android.companion.CompanionDeviceManager
import android.content.Intent
import android.os.Build

@TargetApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
open class UpsideDownCakeUtils() : TiramisuUtils() {
    override fun getBLEDeviceFromIntent(intent : Intent?) : ScanResult?
        = intent?.getParcelableExtra(
              CompanionDeviceManager.EXTRA_ASSOCIATION,
              AssociationInfo::class.java
          )?.getAssociatedDevice()?.getBleDevice()
}



package com.matt.bolton.androidversion

import java.io.Serializable

import android.app.Activity
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.le.ScanResult
import android.companion.CompanionDeviceManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher

val versionUtils : AndroidVersionUtils by lazy {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
        UpsideDownCakeUtils()
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
        TiramisuUtils()
    } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
        SnowConeUtils()
    } else {
        OreoUtils()
    }
}

abstract class AndroidVersionUtils() {
    abstract fun <T : Serializable> getSerializable(
        bundle : Bundle?, key : String, clazz : Class<T>
    ) : T?

    abstract fun writeCharacteristic(
        bluetoothGatt : BluetoothGatt?,
        characteristic : BluetoothGattCharacteristic,
        value : ByteArray,
        writeType : Int,
    ) : Boolean?

    abstract fun writeDescriptor(
        bluetoothGatt : BluetoothGatt,
        descriptor : BluetoothGattDescriptor,
        value : ByteArray,
    ) : Boolean

    abstract fun hasPostNotificationsPermission(context : Context) : Boolean

    abstract fun requestPostNotifications(
        launcher : ActivityResultLauncher<String>,
    )

    abstract fun shouldShowRequestNotificationPermissionRationale (
        activity : Activity,
    ) : Boolean

    abstract fun disassociateAllDevices(deviceManager : CompanionDeviceManager)

    /**
     * Check if Bluetooth permission request needed
     */
    abstract fun needsBluetoothPermissionRequest(context : Context) : Boolean

    /**
     * Runtime Bluetooth permissions that need requesting
     */
    abstract fun requestBluetooth(
        launcher : ActivityResultLauncher<Array<String>>
    )

    /**
     * If the map of granted permissions means Bluetooth is ok
     */
    abstract fun hasBluetooth(granted : Map<String, Boolean>) : Boolean

    /**
     * Get BLE scan result from companion select device launcher
     *
     * @param intent the result intent from the companion launch
     */
    abstract fun getBLEDeviceFromIntent(intent : Intent?) : ScanResult?
}


package com.matt.bolton.androidversion

import java.io.Serializable

import android.app.Activity
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothGattCharacteristic
import android.bluetooth.BluetoothGattDescriptor
import android.bluetooth.le.ScanResult
import android.companion.CompanionDeviceManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.result.ActivityResultLauncher
import androidx.core.content.IntentCompat

open class OreoUtils() : AndroidVersionUtils() {
    @Suppress("deprecation", "unchecked_cast")
    override fun <T : Serializable> getSerializable(
        bundle : Bundle?, key : String, clazz : Class<T>
    ) : T? = bundle?.getSerializable(key) as T?

    @Suppress("deprecation")
    override fun writeCharacteristic(
        bluetoothGatt : BluetoothGatt?,
        characteristic : BluetoothGattCharacteristic,
        value : ByteArray,
        writeType : Int,
    ) : Boolean? {
        characteristic.value = value
        if (bluetoothGatt == null) {
            return null
        } else {
            try {
                return bluetoothGatt.writeCharacteristic(characteristic)
            } catch (e : SecurityException) {
                return false
            }
        }
    }

    @Suppress("deprecation")
    override fun writeDescriptor(
        bluetoothGatt : BluetoothGatt,
        descriptor : BluetoothGattDescriptor,
        value : ByteArray,
    ) : Boolean {
        try {
            descriptor.value = value
            return bluetoothGatt.writeDescriptor(descriptor)
        } catch (e : SecurityException) {
            return false
        }
    }

    override fun hasPostNotificationsPermission(
        context : Context
    ) : Boolean = true

    override fun requestPostNotifications(
        launcher : ActivityResultLauncher<String>,
    ) {
        // do nothing
    }

    override fun shouldShowRequestNotificationPermissionRationale (
        activity : Activity,
    ) : Boolean = false

    @Suppress("deprecation")
    override fun disassociateAllDevices(
        deviceManager : CompanionDeviceManager,
    ) {
        for (oldMac in deviceManager.getAssociations())
            deviceManager.disassociate(oldMac)
    }

    override fun needsBluetoothPermissionRequest(
        context : Context
    ) : Boolean = false

    override fun requestBluetooth(
        launcher : ActivityResultLauncher<Array<String>>
    ) {
        // do nothing
    }

    override fun hasBluetooth(granted : Map<String, Boolean>) : Boolean = true

    @Suppress("deprecation")
    override fun getBLEDeviceFromIntent(intent : Intent?) : ScanResult? {
        if (intent == null) {
            return null
        } else {
            return IntentCompat.getParcelableExtra(
                intent,
                CompanionDeviceManager.EXTRA_DEVICE,
                ScanResult::class.java
            )
        }
    }
}
